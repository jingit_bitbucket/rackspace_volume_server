from django.db import models

class VolRequest(models.Model):
    states = (
        ('B', 'Building'),
        ('Z', 'Done'),
        ('F', 'Building File System'),
        ('S', 'No space'),
        ('E', 'Other Error'),
    )
    units = (
        ('B', 'bytes'),
        ('K', 'kilobytes'),
        ('M', 'megabytes'),
        ('G', 'gigabytes'),
        ('T', 'terabytes'),
        ('P', 'petabytes'),
        ('E', 'exabytes'),
    )
    keyrepr = (
        ('H', 'hex'),
        ('B', 'base64')
    )
    volid = models.CharField(max_length = 32)
    state = models.CharField(max_length = 1, choices = states)
    size = models.IntegerField()
    unit = models.CharField(max_length = 1, choices = units)
    fs = models.CharField(max_length = 32)
    key = models.CharField(max_length = 128)
    key_encoding = models.CharField(max_length=1, choices=keyrepr)
    crypto = models.CharField(max_length=16)
    owner = models.ForeignKey('auth.User', related_name='volrequests')

    def save(self, *args, **kwargs):
        # validate that the request is well formed?
        super(VolRequest, self).save(*args, **kwargs)

    class Meta:
        ordering = ('volid',)
