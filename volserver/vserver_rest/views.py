from django.shortcuts import render
from pprint import pformat
from django.http import HttpResponse
from django.views.generic import View
from pki.cacert import user_dict_from_dn
from django.contrib.auth.models import User
from vserver_rest.models import VolRequest
from rest_framework import generics, permissions, renderers, viewsets
from rest_framework.decorators import api_view, link
from vserver_rest.serializers import VolRequestSerializer, UserSerializer

class VolRequestViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions.
    """

    queryset = VolRequest.objects.all()
    serializer_class = VolRequestSerializer

    def pre_save(self, obj):
        obj.owner = self.request.user

class UserViewSet(viewsets.ReadOnlyModelViewSet):
    """
    This viewset automatically provides `list` and `detail` actions.
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer

@api_view(('GET',))
def api_root(request, format=None):
    return Response({
        'users': reverse('user-list', request=request, format=format),
        'volrequests' : reverse('volrequest-list', request=request, format=format),
    })
