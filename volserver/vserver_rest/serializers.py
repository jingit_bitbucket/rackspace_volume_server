from django.forms import widgets
from rest_framework import serializers
from vserver_rest.models import VolRequest
from django.contrib.auth.models import User

class VolRequestSerializer(serializers.HyperlinkedModelSerializer):
    owner = serializers.Field(source='owner.username')

    class Meta:
        model = VolRequest
        fields = ('url', 'owner', 'volid', 'state', 'size', 'unit',
                  'fs', 'key', 'key_encoding', 'crypto')

class UserSerializer(serializers.HyperlinkedModelSerializer):
    volrequests = serializers.HyperlinkedRelatedField(many=True, view_name='volrequests')

    class Meta:
        model = User
        fields = ('url', 'username', 'volrequests')
