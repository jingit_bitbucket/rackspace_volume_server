from django.conf.urls import patterns, include, url
from vserver_rest import views
from rest_framework.routers import DefaultRouter

API_VERSION='v1'

router = DefaultRouter()
router.register(r'v1/users', views.UserViewSet)
router.register(r'v1/volrequests', views.VolRequestViewSet)

urlpatterns = patterns('',
    url(r'^', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
)
