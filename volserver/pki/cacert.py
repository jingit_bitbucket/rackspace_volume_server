"""
Utility function to extract CN information to provide client ID
"""

def _dictify_dn(dn):
    return dict(x.split('=') for x in dn.split('/') if '=' in x)

def user_dict_from_dn(dn):
    d = _dictify_dn(dn)
    ret = dict()
    ret['username'] = d['CN']
    ret['email'] = ''
    print repr(ret)
    return ret
