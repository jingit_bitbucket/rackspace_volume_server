# README #

This is a rough and ready server which creates, assigns and deletes encrypted volumes on a logical volume and attaches them to virtual machines under KVM.

Authentication is performed via client certificates. The mapping to usernames is very primitive - the CN inside the subject name becomes the username.

Python/Django knows nothing of TLS and/or certs - those are provided as environmental proxy headers to gunicorn, which runs the WSGI service

### What is this repository for? ###

* Version 0.1

### How do I get set up? ###

* Summary of set up

You'll need django REST framework, as well as gunicorn and nginx to proxy requests to the service. The service itself needs to run at the hypervisor level on the host - at the moment, with root privileges :(

* Configuration

config nginx.conf
generate client and server keys/certs

* Dependencies

tbd 

* Database configuration

tbd

* How to run tests

tbd

* To run the service

gunicorn volserver.wsgi

* Deployment instructions



### Contribution guidelines ###

tbd

### Who do I talk to? ###

* Neil Dunbar <ndunbar@jingit.com>